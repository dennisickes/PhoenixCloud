# PhoenixCloud

PhoenixCloud is a Minecraft server/proxy management system. It manages all minecraft server instances, by own developed algorithms, the PhoenixCloud also manages the use of server resources (CPU & RAM). PhoenixCloud has his own plugin-manager, through that other developers can write their own plugins and add those to the PhoenixCloud additionally it's more easily to use due to the included API which contains a full documentation of the content.

# Features
- **Automatic and dynamic starting** of Minecraft server instances depending on the server resources
- **Powerful API** with a full **java documentation**
- **Synchronized** servers over your whole network in real-time
- **High effiency:** We care a lot about effiency, so the software uses server resources as little as possible
- **Multi-Root-System**: You can synchronize many physical or virtual server to one system. Thats possible through the server-wrapper system.
- **Multi-Proxy-System**: Through the round-robin-method you can synchronize more BungeeCord server then one to one server.

# PATCH NOTES
- 1.0 SNAPSHOT - General REALEASE [UNKNOWN]
